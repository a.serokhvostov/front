import React, { Component } from "react";
import { recipeService } from '@/_services';

export default class AddRecipe extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeCookingTime = this.onChangeCookingTime.bind(this);
		this.onChangeIngredients = this.onChangeIngredients.bind(this);
		this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeImageId= this.onChangeImageId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            id: null,
            name: "",
            cookingTime: 0,
            ingredients: "",
			description: "",
            imageId: 0,

            submitted: false
        };
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeCookingTime(e) {
        this.setState({
            cookingTime: e.target.value
        });
    }

    onChangeIngredients(e) {
        this.setState({
            ingredients: e.target.value
        });
    }
	onChangeDescription(e) {
        this.setState({
            description: e.target.value
        });
    }
	onChangeImageId(e) {
        this.setState({
            imageId: e.target.value
        });
    }

    saveEntity() {
        var data = {
            name: this.state.name,
            cookingTime: this.state.cookingTime,
            ingredients: this.state.ingredients,
			description: this.state.description,
            imageId: this.state.imageId
        };

        recipeService.createEntity(data)
            .then(response => {
                this.setState({
                    id: response.id,
                    name: response.name,
                    cookingTime: response.cookingTime,
					ingredients: response.ingredients,
					description: response.description,
					imageId: response.imageId,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({
            id: null,
            name: response.name,
            incidenceRate: response.incidenceRate,
            mortality: response.mortality,

            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            Add
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    required
                                    value={this.state.name}
                                    onChange={this.onChangeName}
                                    name="name"
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="cookingTime">cookingTime</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="cookingTime"
                                    required
                                    value={this.state.cookingTime}
                                    onChange={this.onChangeCookingTime}
                                    name="cookingTime"
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="ingredients">ingredients</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="ingredients"
                                    required
                                    value={this.state.ingredients}
                                    onChange={this.onChangeIngredients}
                                    name="ingredients"
                                />
                            </div>
							<div className="form-group">
                                <label htmlFor="description">description</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="description"
                                    required
                                    value={this.state.description}
                                    onChange={this.onChangeDescription}
                                    name="description"
                                />
                            </div>
							<div className="form-group">
                                <label htmlFor="imageId">imageId</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="imageId"
                                    required
                                    value={this.state.imageId}
                                    onChange={this.onChangeImageId}
                                    name="imageId"
                                />
                            </div>

                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddRecipe };