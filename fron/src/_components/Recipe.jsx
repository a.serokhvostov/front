import React, { Component } from "react";
import { recipeService } from '@/_services';

class Recipe extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeCookingTime = this.onChangeCookingTime.bind(this);
		this.onChangeIngredients = this.onChangeIngredients.bind(this);
		this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeImageId = this.onChangeImageId.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            currentEntity: {
                id: null,
                name: "",
				cookingTime: 0,
				ingredients: "",
				description: "",
				imageId: 0,
            },
            message: "",
        };
    }

    componentDidMount() {
        this.getEntity(this.props.match.params.id);
    }

    onChangeName(e) {
        const name = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    name: name,
                },
            };
        });
    }

    onChangeCookingTime(e) {
        const cookingTime = e.target.value;

        this.setState((prevState) => ({
            currentEntity: {
                ...prevState.currentEntity,
                cookingTime: cookingTime,
            },
        }));
    }

    onChangeIngredients(e) {
        const ingredients = e.target.value;

        this.setState((prevState) => ({
            currentEntity: {
                ...prevState.currentEntity,
                ingredients: ingredients,
            },
        }));
    }
	
	onChangeDescription(e) {
        const description = e.target.value;

        this.setState((prevState) => ({
            currentEntity: {
                ...prevState.currentEntity,
                description: description,
            },
        }));
    }
	
	onChangeImageId(e) {
        const imageId = e.target.value;

        this.setState((prevState) => ({
            currentEntity: {
                ...prevState.currentEntity,
                imageId: imageId,
            },
        }));
    }

    getEntity(id) {
        recipeService.getEntity(id)
            .then((response) => {
				console.log(response);
                this.setState({
                    currentEntity: response,
                });
				this.setState(function (prevState) {
                    const imageId = this.state.currentEntity.imageId.id;
                    return {
                        currentEntity: {
                            ...prevState.currentEntity,
                            imageId: imageId,
                        },
                    };
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    updateEntity() {
        recipeService
            .updateEntity(this.state.currentEntity.id, this.state.currentEntity)
            .then((reponse) => {
                console.log(reponse);

                this.setState({ message: "The recipe was updated successfully!" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    removeEntity() {
        recipeService
            .deleteEntity(this.state.currentEntity.id)
            .then(() => {
                this.props.history.push("/recipe");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        const { currentEntity } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>Recipe</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="title">Title</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    value={currentEntity.name}
                                    onChange={this.onChangeName}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="cookingTime">cookingTime</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="cookingTime"
                                    value={currentEntity.cookingTime}
                                    onChange={this.onChangeCookingTime}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="ingredients">ingredients</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="ingredients"
                                    value={currentEntity.ingredients}
                                    onChange={this.onChangeIngredients}
                                />
                            </div>
							<div className="form-group">
                                <label htmlFor="description">description</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="description"
                                    value={currentEntity.description}
                                    onChange={this.onChangeDescription}
                                />
                            </div>
							<div className="form-group">
                                <label htmlFor="imageId">imageId</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="imageId"
                                    value={currentEntity.imageId}
                                    onChange={this.onChangeImageId}
                                />
                            </div>
							
                        </form>

                        <button
                            className="badge badge-light mr-2"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>

                    
                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a Recipe...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { Recipe };
